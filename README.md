

#tecnew

## Pre-requisites

* Node - http://nodejs.org/
* Grunt - https://gruntjs.com/

## Install

**Before continuing**, please edit the `accountName` key to the `package.json` file. For example:

```json
    {
      "name": "vtex-speed",
      "accountName": "your-store-account-name",
    }
```

Enter the folder you cloned or downloaded, install dependencies and run `npm start`:

```shell
    cd speed
    npm install
    npm start
```
MIT © [VTEX](https://github.com/vtex)
