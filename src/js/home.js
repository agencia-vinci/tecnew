window.onload = function(){
//CARREGA MARCAS


const marcaMenuHome = document.querySelector('.marcas-lista-home');
if (marcaMenuHome) {
    const settingsMarcaHome = {
        "url": "/api/catalog_system/pub/brand/list",
        "method": "GET",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
        },
    };


    $.ajax(settingsMarcaHome).done(function(response) {


        /** ordenação  ....  Marcas*/
        response.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            }
            if (a.name < b.name) {
                return -1;
            }
            // a must be equal to b
            return 0;
        });

        /**  fim ordenação .... Marcas */

        for (response of response) {
            if (response.imageUrl) {
                marcaMenuHome.insertAdjacentHTML(
                    'beforeend',
                    `<li><a href="${response.name}" class="marca-link">
                        <img src="/arquivos/ids${response.imageUrl}">
                    </a></li>`
                );
            }
        }
            var $showCaseOwlMarcas= $(".showcase--marcas ul");
        if ($showCaseOwlMarcas.length) {
            $showCaseOwlMarcas.find('.helperComplement').remove();
            $showCaseOwlMarcas.owlCarousel({
                items: 6,
                autoPlay: true,
                stopOnHover: true,
                pagination: true,
                itemsDesktop: [1199, 6],
                itemsDesktopSmall: [980, 4],
                itemsTablet: [768, 2],
                itemsMobile: [479, 1],
                navigation: true,
                navigationText: ['', ''],
            });
        }

    });





}

}