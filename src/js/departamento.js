document.querySelector('.field-de').addEventListener('keyup', function() {
    let val = this.value
    this.value = ''
    val = val.replace(/[a-z]/gi, '')

    this.value = val
})

document.querySelector('.field-ate').addEventListener('keyup', function() {
    let val = this.value
    this.value = ''
    val = val.replace(/[a-z]/gi, '')

    this.value = val
})

document.querySelector('.btn-send-filter').addEventListener('click', function() {
    const de = document.querySelector('.field-de').value
    const ate = document.querySelector('.field-ate').value

    if (!de || !ate) {
        alert('Preencha os campos corretamente')
        return
    }

    if (de > ate) {
        alert('Insira valores válidos')
        return
    }

    const location = window.location.href.split('/de-')[0] + `/de-${de}-a-${ate}?map=c,priceFrom`
    console.log(window.location.href.split('/de-')[0])
    window.location = location;

})

$('.prateleira .prateleira > ul').attr('class', 'col-xs-6 col-sm-6 col-md-4')
$('.prateleira > .prateleira').addClass('row')