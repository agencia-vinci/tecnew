if (!String.prototype.slugify) {
    String.prototype.slugify = function() {

        return this.toString().toLowerCase()
            .replace(/[àÀáÁâÂãäÄÅåª]+/g, 'a') // Special Characters #1
            .replace(/[èÈéÉêÊëË]+/g, 'e') // Special Characters #2
            .replace(/[ìÌíÍîÎïÏ]+/g, 'i') // Special Characters #3
            .replace(/[òÒóÓôÔõÕöÖº]+/g, 'o') // Special Characters #4
            .replace(/[ùÙúÚûÛüÜ]+/g, 'u') // Special Characters #5
            .replace(/[ýÝÿŸ]+/g, 'y') // Special Characters #6
            .replace(/[ñÑ]+/g, 'n') // Special Characters #7
            .replace(/[çÇ]+/g, 'c') // Special Characters #8
            .replace(/[ß]+/g, 'ss') // Special Characters #9
            .replace(/[Ææ]+/g, 'ae') // Special Characters #10
            .replace(/[Øøœ]+/g, 'oe') // Special Characters #11
            .replace(/[%]+/g, 'pct') // Special Characters #12
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text

    };
};
(function() {
    fetch("/no-cache/profileSystem/getProfile") //pega usuario na loja
        .then((resp) => resp.json())
        .then(function(data) {
            if (data.IsUserDefined) { //verifica se tem usuario ou nao
                if (data.FirstName) { //adicina o nome no header
                    document.querySelector('.profile-controls .login .msg').innerHTML = `Olá, <span>${data.FirstName}</span>`;
                } else {
                    document.querySelector('.login > p').innerHTML = `Olá, <span>${data.Email.split('@')[0]}</span>`;
                }
            } else {
                document.querySelector('.profile-controls .login .msg').innerHTML = `IDENTIFICAÇÃO`;
            }
        });
})();

//CARREGA MENU
const sidebarMenu = document.querySelector('.categorias .menu-container');
if (sidebarMenu) {
    //settings api
    const settings = {
        "url": "/api/catalog_system/pub/category/tree/3/",
        "method": "GET",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
    };

    //building menu
    $.ajax(settings).done(function(response) {
        for (response of response) {
            loadMenu(response)
        }

        initMobile();
    })

    //LOAD MENU
    function loadMenu(menu) {
        let child = menu.children;
        const ulElement = document.createElement('ul');
        const liElement = document.createElement('li');
        const linkElement = document.createElement('a');
        const imgElement = document.createElement('img');
        const spanElement = document.createElement('span');

        imgElement.setAttribute('src', "/arquivos/iconmenu-" + menu.name.slugify() + ".png");
        linkElement.setAttribute('href', menu.url);
        spanElement.textContent = menu.name;
        liElement.setAttribute('class', 'menu-pai');
        linkElement.setAttribute('class', 'link-main-menu');
        linkElement.appendChild(imgElement)
        linkElement.appendChild(spanElement)
        liElement.appendChild(linkElement)
        sidebarMenu.appendChild(liElement)

        for (child of child) {
            const subLiElement = document.createElement('li');
            const subLinkElement = document.createElement('a');

            subLiElement.setAttribute('class', 'sub-menu-item');
            subLinkElement.setAttribute('href', child.url);
            ulElement.setAttribute('class', 'sub-menu');
            subLinkElement.textContent = child.name;

            subLiElement.appendChild(subLinkElement)
            ulElement.appendChild(subLiElement)
            liElement.appendChild(ulElement)
        }
    }
}

//CARREGA MARCAS
const marcaMenu = document.querySelector('.marcas .menu-container');
if (marcaMenu) {
    const settingsMarca = {
        "url": "/api/catalog_system/pub/brand/list",
        "method": "GET",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json",
        },
    };


    $.ajax(settingsMarca).done(function(response) {


        /** ordenação  ....  Marcas*/
        response.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            }
            if (a.name < b.name) {
                return -1;
            }
            // a must be equal to b
            return 0;
        });

        /**  fim ordenação .... Marcas */

        for (response of response) {
            if (response.imageUrl) {
                marcaMenu.insertAdjacentHTML(
                    'beforeend',
                    `<a href="${response.name}" class="marca-link">
                        <img src="/arquivos/ids${response.imageUrl}">
                    </a>`
                );
            }
        }
    });

}
const screenWidth = window.innerWidth;
console.log(screenWidth)
//animaçao desktop
if(screenWidth > 991){
    //animação header
    window.addEventListener('scroll', function(e) {
        let height = 100 - window.scrollY
        height < 0 ? height = 0 : false;
        document.querySelector('.logo-img').style.width = 145 - (window.scrollY) + 'px';
        document.querySelector('.main-head .controls-container').style.height = height + "px";

        if(window.scrollY > 35){
            this.document.querySelector('.main-head').style.top = '0px';
        }else{
            this.document.querySelector('.main-head').style.top = '35px';
        }
    })
}

function initMobile(){
    if(screenWidth < 992){
        document.querySelectorAll('.sub-menu').forEach(function(item){
            item.insertAdjacentHTML('beforebegin','<span class="open-sub"></span>');
        });

        document.querySelectorAll('.open-sub').forEach(function(item){
            item.addEventListener('click',function(){
                const sub = this.nextSibling;
                $(sub).slideToggle()
            })
        });

        document.querySelector('.main-menu>ul').insertAdjacentHTML(
            'beforeend',
            `   <!-- CRIADO VIA JAVASCRIPT HEADER.JS -->
                <a href="" class="mobilelinks">
                    <img class="icon" src="/arquivos/iconmenu-compra-rapida.png" />
                    Compra Rápida
                </a>
                <a href="" class="mobilelinks">
                    <img class="icon" src="/arquivos/iconmenu-promocao-do-mes.png" />
                    Promoções do Mês
                </a>
                <a href="/account" class="mobilelinks">
                    <img class="icon" src="/arquivos/iconmenu-meu-cadastro.png" />
                    Meu Cadastro
                </a>
                <a href="/account" class="mobilelinks">
                    <img class="icon" src="/arquivos/iconmenu-meus-pedidos.png" />
                    Meus Pedidos
                </a>
                <a href="/account" class="mobilelinks">
                    <img class="icon" src="/arquivos/iconmenu-sair.png" />
                    Sair
                </a>
        `);

        document.querySelector('.main-menu').insertAdjacentHTML(
            'afterbegin',
            `
                <!-- ICONE VIA JAVASCRIPT HEADER.JS -->
                <span class="mobile-toggler">
                    <img src="/arquivos/close.png" />
                </span>
            `
        );

        document.querySelectorAll('.mobile-toggler').forEach(function(item){
            item.addEventListener('click',function(){
                document.querySelector('.main-menu').classList.toggle('active')
            })
        });

        document.querySelector('.container-search .btn-buscar').addEventListener('click',function(){
            document.querySelector('.container-search').classList.toggle('active')
        })
    }
}

