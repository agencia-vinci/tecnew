// jQuery Mask Plugin v1.14.16
// github.com/igorescobar/jQuery-Mask-Plugin
var $jscomp = $jscomp || {};
$jscomp.scope = {};
$jscomp.findInternal = function(a, n, f) { a instanceof String && (a = String(a)); for (var p = a.length, k = 0; k < p; k++) { var b = a[k]; if (n.call(f, b, k, a)) return { i: k, v: b } } return { i: -1, v: void 0 } };
$jscomp.ASSUME_ES5 = !1;
$jscomp.ASSUME_NO_NATIVE_MAP = !1;
$jscomp.ASSUME_NO_NATIVE_SET = !1;
$jscomp.SIMPLE_FROUND_POLYFILL = !1;
$jscomp.defineProperty = $jscomp.ASSUME_ES5 || "function" == typeof Object.defineProperties ? Object.defineProperty : function(a, n, f) { a != Array.prototype && a != Object.prototype && (a[n] = f.value) };
$jscomp.getGlobal = function(a) { return "undefined" != typeof window && window === a ? a : "undefined" != typeof global && null != global ? global : a };
$jscomp.global = $jscomp.getGlobal(this);
$jscomp.polyfill = function(a, n, f, p) {
    if (n) {
        f = $jscomp.global;
        a = a.split(".");
        for (p = 0; p < a.length - 1; p++) {
            var k = a[p];
            k in f || (f[k] = {});
            f = f[k]
        }
        a = a[a.length - 1];
        p = f[a];
        n = n(p);
        n != p && null != n && $jscomp.defineProperty(f, a, { configurable: !0, writable: !0, value: n })
    }
};
$jscomp.polyfill("Array.prototype.find", function(a) { return a ? a : function(a, f) { return $jscomp.findInternal(this, a, f).v } }, "es6", "es3");
(function(a, n, f) { "function" === typeof define && define.amd ? define(["jquery"], a) : "object" === typeof exports && "undefined" === typeof Meteor ? module.exports = a(require("jquery")) : a(n || f) })(function(a) {
    var n = function(b, d, e) {
        var c = {
            invalid: [],
            getCaret: function() {
                try {
                    var a = 0,
                        r = b.get(0),
                        h = document.selection,
                        d = r.selectionStart;
                    if (h && -1 === navigator.appVersion.indexOf("MSIE 10")) {
                        var e = h.createRange();
                        e.moveStart("character", -c.val().length);
                        a = e.text.length
                    } else if (d || "0" === d) a = d;
                    return a
                } catch (C) {}
            },
            setCaret: function(a) {
                try {
                    if (b.is(":focus")) {
                        var c =
                            b.get(0);
                        if (c.setSelectionRange) c.setSelectionRange(a, a);
                        else {
                            var g = c.createTextRange();
                            g.collapse(!0);
                            g.moveEnd("character", a);
                            g.moveStart("character", a);
                            g.select()
                        }
                    }
                } catch (B) {}
            },
            events: function() {
                b.on("keydown.mask", function(a) {
                    b.data("mask-keycode", a.keyCode || a.which);
                    b.data("mask-previus-value", b.val());
                    b.data("mask-previus-caret-pos", c.getCaret());
                    c.maskDigitPosMapOld = c.maskDigitPosMap
                }).on(a.jMaskGlobals.useInput ? "input.mask" : "keyup.mask", c.behaviour).on("paste.mask drop.mask", function() {
                    setTimeout(function() { b.keydown().keyup() },
                        100)
                }).on("change.mask", function() { b.data("changed", !0) }).on("blur.mask", function() {
                    f === c.val() || b.data("changed") || b.trigger("change");
                    b.data("changed", !1)
                }).on("blur.mask", function() { f = c.val() }).on("focus.mask", function(b) {!0 === e.selectOnFocus && a(b.target).select() }).on("focusout.mask", function() { e.clearIfNotMatch && !k.test(c.val()) && c.val("") })
            },
            getRegexMask: function() {
                for (var a = [], b, c, e, t, f = 0; f < d.length; f++)(b = l.translation[d.charAt(f)]) ? (c = b.pattern.toString().replace(/.{1}$|^.{1}/g, ""), e = b.optional,
                    (b = b.recursive) ? (a.push(d.charAt(f)), t = { digit: d.charAt(f), pattern: c }) : a.push(e || b ? c + "?" : c)) : a.push(d.charAt(f).replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&"));
                a = a.join("");
                t && (a = a.replace(new RegExp("(" + t.digit + "(.*" + t.digit + ")?)"), "($1)?").replace(new RegExp(t.digit, "g"), t.pattern));
                return new RegExp(a)
            },
            destroyEvents: function() { b.off("input keydown keyup paste drop blur focusout ".split(" ").join(".mask ")) },
            val: function(a) {
                var c = b.is("input") ? "val" : "text";
                if (0 < arguments.length) {
                    if (b[c]() !== a) b[c](a);
                    c = b
                } else c = b[c]();
                return c
            },
            calculateCaretPosition: function(a) {
                var d = c.getMasked(),
                    h = c.getCaret();
                if (a !== d) {
                    var e = b.data("mask-previus-caret-pos") || 0;
                    d = d.length;
                    var g = a.length,
                        f = a = 0,
                        l = 0,
                        k = 0,
                        m;
                    for (m = h; m < d && c.maskDigitPosMap[m]; m++) f++;
                    for (m = h - 1; 0 <= m && c.maskDigitPosMap[m]; m--) a++;
                    for (m = h - 1; 0 <= m; m--) c.maskDigitPosMap[m] && l++;
                    for (m = e - 1; 0 <= m; m--) c.maskDigitPosMapOld[m] && k++;
                    h > g ? h = 10 * d : e >= h && e !== g ? c.maskDigitPosMapOld[h] || (e = h, h = h - (k - l) - a, c.maskDigitPosMap[h] && (h = e)) : h > e && (h = h + (l - k) + f)
                }
                return h
            },
            behaviour: function(d) {
                d =
                    d || window.event;
                c.invalid = [];
                var e = b.data("mask-keycode");
                if (-1 === a.inArray(e, l.byPassKeys)) {
                    e = c.getMasked();
                    var h = c.getCaret(),
                        g = b.data("mask-previus-value") || "";
                    setTimeout(function() { c.setCaret(c.calculateCaretPosition(g)) }, a.jMaskGlobals.keyStrokeCompensation);
                    c.val(e);
                    c.setCaret(h);
                    return c.callbacks(d)
                }
            },
            getMasked: function(a, b) {
                var h = [],
                    f = void 0 === b ? c.val() : b + "",
                    g = 0,
                    k = d.length,
                    n = 0,
                    p = f.length,
                    m = 1,
                    r = "push",
                    u = -1,
                    w = 0;
                b = [];
                if (e.reverse) {
                    r = "unshift";
                    m = -1;
                    var x = 0;
                    g = k - 1;
                    n = p - 1;
                    var A = function() {
                        return -1 <
                            g && -1 < n
                    }
                } else x = k - 1, A = function() { return g < k && n < p };
                for (var z; A();) {
                    var y = d.charAt(g),
                        v = f.charAt(n),
                        q = l.translation[y];
                    if (q) v.match(q.pattern) ? (h[r](v), q.recursive && (-1 === u ? u = g : g === x && g !== u && (g = u - m), x === u && (g -= m)), g += m) : v === z ? (w--, z = void 0) : q.optional ? (g += m, n -= m) : q.fallback ? (h[r](q.fallback), g += m, n -= m) : c.invalid.push({ p: n, v: v, e: q.pattern }), n += m;
                    else {
                        if (!a) h[r](y);
                        v === y ? (b.push(n), n += m) : (z = y, b.push(n + w), w++);
                        g += m
                    }
                }
                a = d.charAt(x);
                k !== p + 1 || l.translation[a] || h.push(a);
                h = h.join("");
                c.mapMaskdigitPositions(h,
                    b, p);
                return h
            },
            mapMaskdigitPositions: function(a, b, d) {
                a = e.reverse ? a.length - d : 0;
                c.maskDigitPosMap = {};
                for (d = 0; d < b.length; d++) c.maskDigitPosMap[b[d] + a] = 1
            },
            callbacks: function(a) {
                var g = c.val(),
                    h = g !== f,
                    k = [g, a, b, e],
                    l = function(a, b, c) { "function" === typeof e[a] && b && e[a].apply(this, c) };
                l("onChange", !0 === h, k);
                l("onKeyPress", !0 === h, k);
                l("onComplete", g.length === d.length, k);
                l("onInvalid", 0 < c.invalid.length, [g, a, b, c.invalid, e])
            }
        };
        b = a(b);
        var l = this,
            f = c.val(),
            k;
        d = "function" === typeof d ? d(c.val(), void 0, b, e) : d;
        l.mask =
            d;
        l.options = e;
        l.remove = function() {
            var a = c.getCaret();
            l.options.placeholder && b.removeAttr("placeholder");
            b.data("mask-maxlength") && b.removeAttr("maxlength");
            c.destroyEvents();
            c.val(l.getCleanVal());
            c.setCaret(a);
            return b
        };
        l.getCleanVal = function() { return c.getMasked(!0) };
        l.getMaskedVal = function(a) { return c.getMasked(!1, a) };
        l.init = function(g) {
            g = g || !1;
            e = e || {};
            l.clearIfNotMatch = a.jMaskGlobals.clearIfNotMatch;
            l.byPassKeys = a.jMaskGlobals.byPassKeys;
            l.translation = a.extend({}, a.jMaskGlobals.translation, e.translation);
            l = a.extend(!0, {}, l, e);
            k = c.getRegexMask();
            if (g) c.events(), c.val(c.getMasked());
            else {
                e.placeholder && b.attr("placeholder", e.placeholder);
                b.data("mask") && b.attr("autocomplete", "off");
                g = 0;
                for (var f = !0; g < d.length; g++) { var h = l.translation[d.charAt(g)]; if (h && h.recursive) { f = !1; break } }
                f && b.attr("maxlength", d.length).data("mask-maxlength", !0);
                c.destroyEvents();
                c.events();
                g = c.getCaret();
                c.val(c.getMasked());
                c.setCaret(g)
            }
        };
        l.init(!b.is("input"))
    };
    a.maskWatchers = {};
    var f = function() {
            var b = a(this),
                d = {},
                e = b.attr("data-mask");
            b.attr("data-mask-reverse") && (d.reverse = !0);
            b.attr("data-mask-clearifnotmatch") && (d.clearIfNotMatch = !0);
            "true" === b.attr("data-mask-selectonfocus") && (d.selectOnFocus = !0);
            if (p(b, e, d)) return b.data("mask", new n(this, e, d))
        },
        p = function(b, d, e) {
            e = e || {};
            var c = a(b).data("mask"),
                f = JSON.stringify;
            b = a(b).val() || a(b).text();
            try { return "function" === typeof d && (d = d(b)), "object" !== typeof c || f(c.options) !== f(e) || c.mask !== d } catch (w) {}
        },
        k = function(a) {
            var b = document.createElement("div");
            a = "on" + a;
            var e = a in b;
            e || (b.setAttribute(a,
                "return;"), e = "function" === typeof b[a]);
            return e
        };
    a.fn.mask = function(b, d) {
        d = d || {};
        var e = this.selector,
            c = a.jMaskGlobals,
            f = c.watchInterval;
        c = d.watchInputs || c.watchInputs;
        var k = function() { if (p(this, b, d)) return a(this).data("mask", new n(this, b, d)) };
        a(this).each(k);
        e && "" !== e && c && (clearInterval(a.maskWatchers[e]), a.maskWatchers[e] = setInterval(function() { a(document).find(e).each(k) }, f));
        return this
    };
    a.fn.masked = function(a) { return this.data("mask").getMaskedVal(a) };
    a.fn.unmask = function() {
        clearInterval(a.maskWatchers[this.selector]);
        delete a.maskWatchers[this.selector];
        return this.each(function() {
            var b = a(this).data("mask");
            b && b.remove().removeData("mask")
        })
    };
    a.fn.cleanVal = function() { return this.data("mask").getCleanVal() };
    a.applyDataMask = function(b) {
        b = b || a.jMaskGlobals.maskElements;
        (b instanceof a ? b : a(b)).filter(a.jMaskGlobals.dataMaskAttr).each(f)
    };
    k = {
        maskElements: "input,td,span,div",
        dataMaskAttr: "*[data-mask]",
        dataMask: !0,
        watchInterval: 300,
        watchInputs: !0,
        keyStrokeCompensation: 10,
        useInput: !/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent) &&
            k("input"),
        watchDataMask: !1,
        byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
        translation: { 0: { pattern: /\d/ }, 9: { pattern: /\d/, optional: !0 }, "#": { pattern: /\d/, recursive: !0 }, A: { pattern: /[a-zA-Z0-9]/ }, S: { pattern: /[a-zA-Z]/ } }
    };
    a.jMaskGlobals = a.jMaskGlobals || {};
    k = a.jMaskGlobals = a.extend(!0, {}, k, a.jMaskGlobals);
    k.dataMask && a.applyDataMask();
    setInterval(function() { a.jMaskGlobals.watchDataMask && a.applyDataMask() }, k.watchInterval)
}, window.jQuery, window.Zepto);

$(document).ready(function() {

    var storeName = "tecnew"; //Indica o nome da conta utilizada na API do MasterData
    var dataEntity = "CO"; //Indica a sigla da entidade de dados utilizada na API do MasterData
    var htmlElementId = "contactForm"; //Indica o ID do elemento HTML que receberÃ¡ o formulÃ¡rio
    var messageLoading = "Carregando..."; //Mensagem de carregamento do formulÃ¡rio (ao salvar)
    var messageValidation = "Preencha corretamente todos os campos obrigatÃ³rios do formulÃ¡rio."; //Mensagem de validaÃ§Ã£o de formulÃ¡rio
    var messageSuccess = "FormulÃ¡rio enviado com sucesso. Em breve nossa equipe entrarÃ¡ em contato com vocÃª. Obrigado."; //Mensagem de sucesso
    var messageError = "Algum erro aconteceu. Tente novamente mais tarde."; //Mensagem de erro

    FormCreate(storeName, dataEntity, htmlElementId, messageLoading, messageValidation, messageSuccess, messageError);
});

function ContactCreate(storeName, dataEntity, co_client) {
    var co_description = $("#co_description").val();
    var co_type = $("#co_type").val();
    var co_pedido = $("#co_pedido").val();
    var co_empresa = $('#co_empresa').val();
    var co_phone = $('#cl_phone').val();

    var jsonCO = {
        "client": co_client.replace("CL-", ""),
        "description": co_description,
        "type": co_type,
        "empresa": co_empresa,
        "pedido": co_pedido,
        "telefone": co_phone
    };

    var urlCO = "/api/dataentities/CO/documents/";

    $.ajax({
        headers: {
            "Accept": "application/vnd.vtex.ds.v10+json",
            "Content-Type": "application/json"
        },
        data: JSON.stringify(jsonCO),
        type: 'PATCH',
        url: urlCO,
        success: function(data) {
            console.log(data);
            ResetMessages()
            $("#co_message_success").show();
            $("#cl_first_name").val("");
            $("#cl_email").val("");
            $("#cl_phone").val("");
            $("#co_empresa").val("");
            $("#co_type").val("");
            $("#co_pedido").val("");
            $("#co_description").val("");
        },
        error: function(data) {
            console.log(data);
            ResetMessages()
            $("#co_message_error").show();
        }
    });
}

function ContactCreateByEmail(storeName, dataEntity, cl_email) {
    var cl_url = "/api/dataentities/CL/search/?email=" + cl_email + "&_fields=id";

    $.ajax({
        headers: {
            "Accept": "application/vnd.vtex.ds.v10+json",
            "Content-Type": "application/json"
        },
        type: 'GET',
        url: cl_url,
        success: function(data, textStatus, xhr) {
            console.log(data);
            if (xhr.status == "200" || xhr.status == "201") {
                ContactCreate(storeName, dataEntity, data[0].id);
            } else {
                ResetMessages()
                $("#co_message_error").show();
            }
        },
        error: function(data) {
            console.log(data);
            ResetMessages()
            $("#co_message_error").show();
        }
    });
}

function ClientCreate() {
    var storeName = $("#master_data_store_name").val();
    var dataEntity = $("#master_data_data_entity").val();

    var cl_first_name = $("#cl_first_name").val();
    var cl_email = $("#cl_email").val();
    var cl_phone = $("#cl_phone").val();

    var cl_json = {
        "firstName": cl_first_name,
        "email": cl_email,
        "telefone": cl_phone,
    };

    var cl_url = "/api/dataentities/CL/documents/";

    $.ajax({
        headers: {
            "Accept": "application/vnd.vtex.ds.v10+json",
            "Content-Type": "application/json"
        },
        data: JSON.stringify(cl_json),
        type: 'PATCH',
        url: cl_url,
        success: function(data, textStatus, xhr) {
            console.log(data);
            if (xhr.status == "200" || xhr.status == "201") {
                ContactCreate(storeName, dataEntity, data.Id);
            } else if (xhr.status == "304") {
                ContactCreateByEmail(storeName, dataEntity, cl_email);
            } else {
                ResetMessages()
                $("#co_message_error").show();
            }
        },
        error: function(data) {
            console.log(data);
            ResetMessages()
            $("#co_message_error").show();
        }
    });
}

function ResetMessages() {
    $("#co_message_loading").hide();
    $("#co_message_validate").hide();
    $("#co_message_success").hide();
    $("#co_message_error").hide();
}

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function FormValidate() {
    var isFormValidate = true;

    if ($("#cl_first_name").val() == "") {
        isFormValidate = false;
        $("#cl_first_name").focus();
    }
    if ((isFormValidate) && ($("#cl_last_name").val() == "")) {
        isFormValidate = false;
        $("#cl_last_name").focus();
    }
    if ((isFormValidate) && ($("#cl_email").val() == "")) {
        isFormValidate = false;
        $("#cl_email").focus();
    }
    if ((isFormValidate) && (!IsEmail($("#cl_email").val()))) {
        isFormValidate = false;
        $("#cl_email").val("");
        $("#cl_email").focus();
    }
    if ((isFormValidate) && ($("#cl_phone").val() == "")) {
        isFormValidate = false;
        $("#cl_phone").focus();
    }
    if ((isFormValidate) && ($("#co_type").val() == "")) {
        isFormValidate = false;
        $("#co_type").focus();
    }
    if ((isFormValidate) && ($("#co_description").val() == "")) {
        isFormValidate = false;
        $("#co_description").focus();
    }

    if (isFormValidate) {
        ResetMessages()
        $("#co_message_loading").show();
        ClientCreate();
    } else {
        ResetMessages()
        $("#co_message_validate").show();
    }

    return false;
}

function FormCreate(storeName, dataEntity, htmlElementId, messageLoading, messageValidation, messageSuccess, messageError) {
    var htmlContent = '';

    htmlContent += '<div id="co_message_loading" class="alert alert-info" style="display:none;">' + messageLoading + '</div>';
    htmlContent += '<div id="co_message_validate" class="alert alert-warning" style="display:none;">' + messageValidation + '</div>';
    htmlContent += '<div id="co_message_success" class="alert alert-success" style="display:none;">' + messageSuccess + '</div>';
    htmlContent += '<div id="co_message_error" class="alert alert-danger" style="display:none;">' + messageError + '</div>';
    htmlContent += '<div class="info">Campos indicados com * são de preenchimento obrigatório.</div>';
    htmlContent += '<form id="co_form" class="formulario" action="javascript:FormValidate();" method="post">';
    htmlContent += '<input type="hidden" id="master_data_store_name" name="master_data_store_name" value="' + storeName + '" />';
    htmlContent += '<input type="hidden" id="master_data_data_entity" name="master_data_data_entity" value="' + dataEntity + '" />';
    htmlContent += '<div class="form-field string required cl_first_name list obrigatorio form">';
    htmlContent += '<label class="tit_label" for="cl_first_name">Nome*</label>';
    htmlContent += '<input id="cl_first_name" maxlength="100" name="cl_first_name" type="text" required/>';
    htmlContent += '</div>';
    htmlContent += '<div class="form-field string required cl_email list obrigatorio form">';
    htmlContent += '<label class="tit_label" for="cl_email">Email*</label>';
    htmlContent += '<input id="cl_email" maxlength="100" name="cl_email" type="text" required>';
    htmlContent += '</div>';
    htmlContent += '<div class="form-field string required cl_phone list obrigatorio form">';
    htmlContent += '<label class="tit_label" for="cl_phone">Telefone*</label>';
    htmlContent += '<input id="cl_phone" maxlength="100" name="cl_phone" type="text" required/>';
    htmlContent += '<span class="span-input">(DDD + XXXX-XXXX)';
    htmlContent += '</div>';
    htmlContent += '<div class="form-field string required co_pedido list obrigatorio form">';
    htmlContent += '<label class="tit_label" for="co_pedido">Número do Pedido</label>';
    htmlContent += '<input id="co_pedido" maxlength="100" name="co_pedido" />';
    htmlContent += '<span class="span-input">(Caso a dúvida seja de uma compra já realizada)';
    htmlContent += '</div>';
    htmlContent += '<div class="form-field string required co_empresa list obrigatorio form">';
    htmlContent += '<label class="tit_label" for="co_empresa">Empresa</label>';
    htmlContent += '<input id="co_empresa" maxlength="100" name="co_empresa" type="text" />';
    htmlContent += '</div>';
    htmlContent += '<div class="form-field string required co_type list obrigatorio form">';
    htmlContent += '<label class="tit_label" for="co_type">Tipo *</label>';
    htmlContent += '<select name="co_type" id="co_type">'
    htmlContent += '<option value="">-</option>'
    htmlContent += '<option value="Sugestao">Sugestao</option>'
    htmlContent += '<option value="Dúvida">Dúvida</option>'
    htmlContent += '<option value="Reclamação">Reclamação</option>'
    htmlContent += '</select>'
    htmlContent += '</div>';
    htmlContent += '<div class="form-field string required co_description list obrigatorio form">';
    htmlContent += '<label class="tit_label" for="co_description">Qual sua dúvida?*</label>';
    htmlContent += '<textarea id="co_description" name="co_description"></textarea>';
    htmlContent += '</div>';
    htmlContent += '<div class="form-field submit bt-principal"><input id="commit" name="commit" type="submit" value="Enviar"></div>';
    htmlContent += '</form>';

    $("#" + htmlElementId).html(htmlContent);
}

setTimeout(() => {
    $('input#cl_phone').mask('(000) 0000-0000');
}, 1000)